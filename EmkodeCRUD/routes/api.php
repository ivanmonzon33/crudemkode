<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Models\Empleado;
Use App\Http\Controllers\EmpleadoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Ruta para obtener todos los empleados
Route::get('empleados', 'App\Http\Controllers\EmpleadoController@index');
//Ruta para obtener un empleado en específico
Route::get('empleados/{id}', 'App\Http\Controllers\EmpleadoController@show');
//Ruta para guardar un empleado 
Route::post('empleados', 'App\Http\Controllers\EmpleadoController@store');
//Ruta para actualizar todos los empleados
Route::put('empleados/{id}', 'App\Http\Controllers\EmpleadoController@update');
//Ruta para eliminar un empleado 
Route::delete('empleados/{id}', 'App\Http\Controllers\EmpleadoController@delete');
