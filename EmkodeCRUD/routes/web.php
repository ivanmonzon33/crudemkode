<?php

use Illuminate\Support\Facades\Route;
Use App\Models\Empleado;
Use App\Http\Controllers\EmpleadoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','App\Http\Controllers\EmpleadoController@verEmpleados')->name("empleados");
//Ruta para eliminar un empleado
Route::any('/delete/{id_producto}','App\Http\Controllers\EmpleadoController@deleteEmpleado')->name("deleteEmpleado");
//Ruta para obtener los datos un empleado
Route::any('/datosEmpleado/{id_producto}','App\Http\Controllers\EmpleadoController@datosEmpleado')->name("datosEmpleado");
//Ruta para editar un empleado
Route::any('/editarEmpleado','App\Http\Controllers\EmpleadoController@editarEmpleado')->name("editarEmpleado");
//Ruta para guardar un empleado
Route::get('/storeEmpleado/{name}/{lastName}/{email}/{phone}','App\Http\Controllers\EmpleadoController@storeEmpleado')->name("storeEmpleado");


