<!DOCTYPE html> 
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="Autor" content="Carlos Mzn">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CRUD Emkode</title>
    <!--Usamos Booststrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <!--Usamos Fontawesome-->
    <script src="https://kit.fontawesome.com/983801dc2b.js" crossorigin="anonymous"></script>
    <!--Scripts necesarios para la tabla-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</head>
    <body>
        <div class="container-fluid">
            <div class="row bg-dark">
                <div class="col-lg-12">
                    <p class="text-center text-light display-4 pt-2">CRUD Emkode</p>
                    <p class="text-center text-light">Por: Carlos Iván Monzón Chávez</p>
                </div>
            </div>
        </div>
        <div class="container">
            <hr class="bg-info">
            <!--Tabla de usuarios-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row mt-3">
                                <div class="col-lg-6">
                                    <h3 class="text-primary">Empleados&nbsp;<i class="fas fa-users"></i></h3>
                                </div>
                                <div class="col-lg-6">
                                    <button type="button" class="btn btn-primary float-end" data-toggle="modal" data-target="#modalAdd">
                                        Agregar empleado&nbsp;&nbsp;<i class="fas fa-user"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                        <div class="table-responsive">
                            <table id="myTable" class="table table-striped table-hover" >
                            <thead>
                                <tr>
                                <th># <i class="fas fa-user-friends"></i></th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo electrónico</th>
                                <th>Teléfono</th>
                                <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($empleados as $row)
                                <tr>
                                    <td>{!!$row->id!!}</td>
                                    <td>{!!$row->name!!}</td>
                                    <td>{!!$row->last_name!!}</td>
                                    <td>{!!$row->email!!}</td>
                                    <td>{!!$row->phone!!}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEdit" onclick="getDatosEmpleado('{!!$row->id!!}')">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                        &nbsp;
                                        <button type="button" class="btn btn-danger" onclick="deleteEmpleado('{!!$row ->id!!}')">
                                            <i class="fas fa-times"></i>
                                          </button>
                                          
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   

        <!-- Modal Agregar-->
        <div class="modal fade" id="modalAdd" tabindex="-1" aria-labelledby="modalAdd" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar empleado</h4>
                <button type="button" class="btn-close" data-toggle="modal" data-target="#modalAdd" onclick="dismissModalAdd()"></button>
                </div>
                <div class="modal-body p-4">
                    <form id="formAdd">
                        {{csrf_field()}}
                        <div class="mb-3">
                            <input type="text" id = "nameAdd" name="nameAdd" class="form-control form-control-lg" placeholder="Nombre" required>
                        </div>
                        <div class="mb-3">
                            <input type="text" id = "lastAdd" name="lastAdd" class="form-control form-control-lg" placeholder="Apellido" required>
                        </div>
                        <div class="mb-3">
                            <input type="email" id = "emailAdd" name="emailAdd" class="form-control form-control-lg" placeholder="Correo electrónico" required>
                        </div>
                        <div class="mb-3">
                            <input type="tel" id = "phoneAdd" name="phoneAdd" class="form-control form-control-lg" placeholder="Teléfono" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalAdd" onclick="dismissModalAdd()">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="addEmpleado()">Agregar empleado</button>
                </div>
            </div>
            </div>
        </div>
        <!-- Modal Editar-->
        <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Editar empleado</h4>
                <button type="button" class="btn-close" data-toggle="modal" data-target="#modalEdit" onclick="dismissModalEdit()"></button>
                </div>
                <div class="modal-body p-4">
                    <form id="formEditar">
                        {{csrf_field()}}
                        <div class="mb-3">
                            <input type="text" id = "nameEdit" name="nameEdit" class="form-control form-control-lg" placeholder="Nombre" >
                        </div>
                        <div class="mb-3">
                            <input type="text" id = "lastEdit" name="lastEdit" class="form-control form-control-lg" placeholder="Apellido">
                        </div>
                        <div class="mb-3">
                            <input type="email" id = "emailEdit" name="emailEdit" class="form-control form-control-lg" placeholder="Correo electrónico">
                        </div>
                        <div class="mb-3">
                            <input type="tel" id = "phoneEdit" name="phoneEdit" class="form-control form-control-lg" placeholder="Teléfono">
                        </div>
                        <input id="id" name="id" type="number" class="form-control" placeholder="0" min="1" max="100" hidden>
                    </form>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalEdit" onclick="dismissModalEdit()">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="editEmpleado()">Actualizar empleado</button>
                </div>
            </div>
            </div>
        </div>

    <!--Scripts-->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script>
            $(document).ready( function () {
                $('#myTable').DataTable();
            });

            function deleteEmpleado(id){
            swal({
              title: '¿Seguro que desea eliminar el registro?',
              text: "¡Esta accion no es reversible!",
              type: 'warning',
              icon : "error",
              buttons:{
                cancel: {
                  text : 'Cancelar',
                  visible: true,
                  className: 'btn btn-warning'
                },
                confirm: {
                  text : 'Si, deseo eliminarlo!',
                  className : 'btn btn-danger',
                }
                
              }
            }).then((Delete) => {
              if (Delete) {
                $.ajax({
                url:'/delete/'+id,
                type:'get'
                }).done(function (res){
                swal({
                  title: 'Eliminado!',
                  text: 'El registro ha sido Eliminado.',
                  icon : "success",
                  buttons:false,
                  timer:5000,
                });
                  location.reload();
                  });
              } else {
                swal.close(
                );
              }
            });
        

            }


            function editEmpleado(){
                var frm=$("#formEditar");    
                var datos = frm.serialize();
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                
                $.ajax({
                type:'POST',
                url:'/editarEmpleado/',
                data:datos,
                success:function(data){
                    swal({
                        title:"Modificacion Realizada",
                        text: "Modificacion realizada con exito",
                        icon: "success",
                        buttons: false,
                        timer:5000,
                        });
                    },
                complete: function(){
                    
                    location.reload();
                    }

                    });                
            }

            function addEmpleado(){
                var name = document.getElementById("nameAdd").value;
                var lastName = document.getElementById("lastAdd").value;
                var email = document.getElementById("emailAdd").value;
                var phone = document.getElementById("phoneAdd").value;
                //Petición AJAX a la base de datos con parámetros.
                $.ajax({
                url: '/storeEmpleado/'+name+'/'+lastName+'/'+email+'/'+phone,
                type:"GET",
                dataType:"json",
                    success:function(data) {
                        $("#modalAdd").modal('hide');
                            },
                    complete: function(){
                        swal({
                              title:"Operación exitosa",
                              text:"Empleado agregado con éxito",
                              icon: "success",
                              button: false, 
                              timer: 5000,
                            });
                            location.reload();
                    }
                    
                    });           
                
                
            }

            function getDatosEmpleado(id){
                $.ajax({
                url:'/datosEmpleado/'+id,
                type:'get'
                }).done(function (res){
                $("#id").val(res.datos[0].id); 
                $("#nameEdit").val(res.datos[0].name);    
                $('#lastEdit').val(res.datos[0].last_name);
                $('#emailEdit').val(res.datos[0].email);
                $('#phoneEdit').val(res.datos[0].phone);
                $("#modalEdit").modal("show");
                
                });
            }
            
            
        </script>
        
        
        
    </body>
</html>
