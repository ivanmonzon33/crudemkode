<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Empleado;
Use App\Http\Controllers\EmpleadoController;
use Illuminate\Support\Facades\DB;

class EmpleadoController extends Controller
{
    public function index()
    {
        return Empleado::all();
    }
 
    public function show($id)
    {
        return Empleado::find($id);
    }

    public function storeEmpleado($name, $lastName, $email, $phone)
    {
      $empleados=DB::table('empleados')->select('*')->get();
      $nuevaEmpleado=new Empleado();
      $nuevaEmpleado->name=$name;
      $nuevaEmpleado->last_name=$lastName;
      $nuevaEmpleado->email=$email;
      $nuevaEmpleado->phone=$phone;
      $nuevaEmpleado->save();
      return view("welcome",compact('empleados'));
    }

    public function editarEmpleado(Request $request)
    {
        if($request->ajax()){
            $v = \Validator::make($request->all(), [
                 'nameEdit' => 'required',
                 'lastEdit' => 'required',
                 'emailEdit'    => 'required',
                 'phoneEdit'    => 'required'
                ]);
            if ($v->fails()){
              echo $v->errors();
              return response()->json(['errors'=>$v->errors()->all()]);
              //return Response::json(array('errors' => $v->getMessageBag()->toArray()));
            } else {
              $empleado=Empleado::find($request->id);
              $empleado->name=$request->nameEdit;
              $empleado->last_name=$request->lastEdit;
              $empleado->email=$request->emailEdit;
              $empleado->phone=$request->phoneEdit;
              $empleado->save();          
            }
          }
    }

    public function delete(Request $request, $id)
    {
        $empleado = Empleado::findOrFail($id);
        $empleado->delete();

    }

    public function verEmpleados(){
        $empleados=DB::table('empleados')->select('*')->get();
        return view("welcome",compact('empleados'));
    }

    public function deleteEmpleado($id){
        $empleados=DB::table('empleados')->select('*')->get();
        $empleado = DB::table('empleados')
        ->where('id', '=', $id)
        ->delete();
        return view("welcome",compact('empleados'));
    }

    public function datosEmpleado($id){
        $datos = DB::table('empleados')
        ->where('id', '=', $id)
        ->select('*')
        ->get();
        return ($datos=array('datos' => $datos));
    }
}
